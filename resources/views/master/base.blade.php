<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>WorldPols</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	<link href="{{ asset('/css/animate.css') }}" rel="stylesheet">
	<link rel="apple-touch-icon" sizes="57x57" href="/images/icons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/images/icons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/images/icons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/images/icons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/images/icons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/images/icons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/images/icons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/images/icons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/images/icons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/images/icons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/images/icons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon-16x16.png">
	<link rel="manifest" href="/images/icons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/images/icons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	@yield('styles')

	<link href="{{ asset('/css/style.css') }}" rel="stylesheet">

	<!-- Fonts -->
	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<section id="header" class="wow fadeInUp">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-world">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/">{{ $setting['site-name'] }}</a>
					</div>
			
					<div class="collapse navbar-collapse" id="navbar-world">
						<ul class="nav navbar-nav">
							<li @if ($pagename == 'home') class="active" @endif><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
							<li @if ($pagename == 'about-us') class="active" @endif><a href="{{ url('/about-us') }}"><i class="fa fa-users"></i> About Us</a></li>
							<li @if ($pagename == 'contribute') class="active" @endif><a href="{{ url('/contribute') }}"><i class="fa fa-user-plus"></i> Contribute</a></li>
							<li @if ($pagename == 'contact-us') class="active" @endif><a href="{{ url('/contact-us') }}"><i class="fa fa-envelope"></i> Contact Us</a></li>
						</ul>
			
						<ul class="nav navbar-nav navbar-right media-icons">
							<li><a href="{{ url($setting['facebook']) }}" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a></li>
							<li><a href="{{ url($setting['twitter']) }}" target="_blank"><i class="fa fa-twitter-square fa-2x"></i></a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</section>

	
	@yield('content')


	<section id="footer" class="wow fadeInUp">
		<div class="container">
			<div class="col-md-12">
				<p>All Rights Reserved : 2015</p>
			</div>
		</div>
	</section>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="{{ asset('js/wow.min.js') }}"></script>
	<script type="text/javascript">
		$(function() {
			new WOW().init();

			
		});
		
	</script>

	@yield('scripts')
</body>
</html>
