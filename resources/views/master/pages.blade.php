@extends('master.base')

@section('content')

	<section id="sub-banner">
		<div class="container">
			<div class="col-md-12">
				<h2 class="page-title">@yield('page-title')</h2>
			</div>
		</div>
	</section>

	<section id="crumbs">
		<div class="container">
			<div class="col-md-12">
				<div class="row">@yield('crumbs')</div>
			</div>
		</div>
	</section>
	
	@yield('subcontent')
@stop