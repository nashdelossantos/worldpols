@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Dashboard</div>

				<div class="panel-body">
					<div class="col-md-12">
						<div class="col-md-6">
							<h3>Total</h3>
							<p>Countries : <span id="total-countries"></span></p>
							<p>Appointments : <span id="total-governors">{{ $totals['total_appoints'] }}</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
