@extends('master.pages')

@section('content')
	<section id="subpage">
		<div class="container">
			<div class="col-md-12">
				<div class="title_page wow fadeInUp">
					<h3 class="text-right">Success</h3>
				</div>
			</div>
		</div>
	</section>

	<section id="pagecontent">
		<div class="container">
			<div class="col-md-12">
				<div class="item">
					<h3>Email Sent</h3>
						
					<p>{{ $message }}</p>
				</div>
			</div>
		</div>
	</section>
@stop