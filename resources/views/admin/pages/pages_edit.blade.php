@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Editing Page
					<div class="action-btns pull-right">
						<div class="btn-group">
							<a href="{{ url('/admin/pages') }}" class="btn btn-sm btn-default"><i class="fa fa-arrow-circle-o-left"></i> Cancel</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

						
						{!! Form::model($pages, ['url' => 'admin/pages/' . $pages->slug, 'method' => 'PATCH']) !!}
							<div class="col-md-12">
								<h5>Person Details</h5>
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Page Title : </label>
									<div class="col-md-9">
										{!! Form::text('page_title', null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Page Contents : </label>
									<div class="col-md-9">
										{!! Form::textarea('page_content', null, ['class' => 'form-control', 'id' => 'title']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group text-right">
									 <div class="col-md-12">
									 	<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Update Page</button>
									 </div>

								</div>

							</div>
							
						{!! Form::close() !!}
							
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script src="//cdn.ckeditor.com/4.4.7/basic/ckeditor.js"></script>
	<script>CKEDITOR.replace('title');</script>
@stop
