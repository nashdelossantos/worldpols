@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Pages
					<div class="action-btns pull-right hidden">
						<div class="btn-group">
							<a href="{{ url('/admin/pages/create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> New</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
						<div class="col-md-6">
							<div class="body-item">
								<ul>
									@foreach ($pages as $page)
										<li><a href="{{ url('/admin/pages/'.$page->slug.'/edit') }}"> {{ $page->page_title }}</a></li>
									@endforeach
								</ul>

							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
