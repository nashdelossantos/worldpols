@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Creating New Appointments
					<div class="action-btns pull-right">
						<div class="btn-group">
							<a href="{{ url('/admin/appointment') }}" class="btn btn-sm btn-default"><i class="fa fa-arrow-circle-o-left"></i> Cancel</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

						
						{!! Form::model($appointment, ['url' => 'admin/appointment/' . $appointment->slug, 'files' => true, 'method' => 'PATCH']) !!}
							<div class="col-md-6">
							<h5>Person Details</h5>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Full Name : </label>
									<div class="col-md-8">
										{!! Form::text('full_name', null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Position : </label>
									<div class="col-md-8">
										{!! Form::select('position_id', $positionarr, null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Party : </label>
									<div class="col-md-8">
										<input type="text" name="party" class="form-control">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Inauguration Year 1 : </label>
									<div class="col-md-8">
										{!! Form::selectRange('term_a', 1990,2015, Input::old('term_a'), ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Inauguration Year 2 : </label>
									<div class="col-md-8">
										{!! Form::selectRange('term_b', 1990,2015, Input::old('term_b'), ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Inauguration Month : </label>
									<div class="col-md-8">
										{!! Form::selectMonth('month', Input::old('month'), ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Time In Office : </label>
									<div class="col-md-8">
										{!! Form::selectRange('office_time', 1,5, Input::old('office_time'), ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Term : </label>
									<div class="col-md-8">
										{!! Form::selectRange('term', 1,5, Input::old('term'), ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Elected ?</label>
									<div class="col-md-8">
										{!! Form::label('elect-0', 'Yes') !!}	
										{!! Form::radio('elect', 'yes') !!}

										{!! Form::label('elect-1', 'No') !!}	
										{!! Form::radio('elect', 'no') !!}
									</div>
								</div>

							</div>
							<div class="col-md-6">
								<div class="photo-holder">
									{!! Form::file('avatar') !!}
									<div class="photo-overlay">
										<h3>Upload Photo</h3>
										<img src="{{ asset('images/people')}}/{{ $appointment->avatar }}" alt="">
									</div>
								</div>
								<h5>Place of Appointment</h5>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Country : </label>
									<div class="col-md-8">
										{!! Form::select('country_id', $country, null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">State : </label>
									<div class="col-md-8">
										{!! Form::select('state', $states, null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Capital : </label>
									<div class="col-md-8">
										{!! Form::text('capital', null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Region : </label>
									<div class="col-md-8">
										{!! Form::text('region', null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group text-right">
									 <div class="col-md-12">
									 	<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Add Appointment</button>
									 </div>

								</div>
							</div>
							
						{!! Form::close() !!}
							
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$("input:file").change(function (){
		   	$('.photo-overlay h3').text('Ready for upload...')
		});
	</script>
@stop
