@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Appointments 
					<span class="label label-primary">Governor</span>
					<span class="label label-warning">House of Representative</span>
					<span class="label label-danger">President</span>
					<span class="label label-info">Senator</span>
					<span class="label label-default">Un-assigned</span>
					<div class="action-btns pull-right">
						<div class="btn-group">
							<a href="{{ url('/admin/appointment/create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> New</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="body-item">
								<h5>List of Appointments (click to edit)</h5>
								<div class="appointlist">
									<div class="col-md-4">
									<?php $counter = 1; ?>
									@foreach ($appointments as $key => $appoint)
										<a class="btn @if ($appoint->position_id == 1)btn-primary @elseif ($appoint->position_id == 2) btn-warning @elseif ($appoint->position_id == 4) btn-danger  @elseif ($appoint->position_id == 3) btn-info @else btn-default @endif btn-block" href="{{ url('/admin/appointment/'.$appoint->slug.'/edit') }}"> {{ $appoint->full_name }}</a>

										@if ($counter >= $collimit) 
											<?php $counter = 1; ?>
											</div> <!-- close col-md-4  and start new column -->
											<div class="col-md-4">
										@else
											<?php $counter++; ?>
										@endif
									@endforeach
									</div> <!-- end final column -->
								</div>
							</div>
							
						</div>
					</div>
				</div>

				<div class="pagination text-center">
					<div class="col-md-12">
						<div class="btn-group">
							@foreach (range('A', 'Z') as $chars)
								<a href="{{ asset('admin/appointment/page/')}}/{{ strtolower($chars) }}" class="btn @if ($chars == ucfirst($currpage)) btn-primary @else btn-default @endif">{{ $chars }}</a>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
