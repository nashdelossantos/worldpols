@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Creating New Appointments
					<div class="action-btns pull-right">
						<div class="btn-group">
							<a href="{{ url('/admin/appointment') }}" class="btn btn-sm btn-default"><i class="fa fa-arrow-circle-o-left"></i> Cancel</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
						
						{!! Form::open(array('url' => '/admin/appointment', 'files' => true)) !!}
							<div class="col-md-6">
							<h5>Person Details</h5>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Full Name : </label>
									<div class="col-md-8">
										<input type="text" name="full_name" class="form-control">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Position : </label>
									<div class="col-md-8">
										{!! Form::select('position', $positionarr, old('position'), ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Party : </label>
									<div class="col-md-8">
										<input type="text" name="party" class="form-control">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Inauguration Year 1 : </label>
									<div class="col-md-8">
										<select name="term_a" class="form-control">
											<option value="0">Choose a year</option>
											@for ($i = 1990; $i < 2015; $i++)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor 
										</select>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Inauguration Year 2 : </label>
									<div class="col-md-8">
										<select name="term_b" class="form-control">
											<option value="0">Choose a year</option>
											@for ($i = 1990; $i < 2015; $i++)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor 
										</select>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Inauguration Month : </label>
									<div class="col-md-8">
										{!! Form::selectMonth('month', '1', ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Time In Office : </label>
									<div class="col-md-8">
										{!! Form::selectRange('office_time', 1,5, null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Term : </label>
									<div class="col-md-8">
										<select name="term" class="form-control">
											<option value="0" name="term">Choose a term</option>
											@for ($i = 1; $i < 5; $i++)
												<option value="{{ $i }}">{{ $i }} Term</option>
											@endfor 
										</select>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Elected ?</label>
									<div class="col-md-8">
										<label class="radio-inline">
											<input type="radio" name="elect" value="yes"> Yes
										</label>
										<label class="radio-inline">
											<input type="radio" name="elect" value="no"> No
										</label>
									</div>
								</div>

							</div>
							<div class="col-md-6">
								<div class="photo-holder">
									{!! Form::file('avatar') !!}
									<div class="photo-overlay">
										<h3>Upload Photo</h3>
									</div>
								</div>
								
								<h5>Place of Appointment</h5>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Country : </label>
									<div class="col-md-8">
										{!! Form::select('country', $country, null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">State : </label>
									<div class="col-md-8">
										{!! Form::select('state', $states, null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Capital : </label>
									<div class="col-md-8">
										<input type="text" name="capital" class="form-control">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Region : </label>
									<div class="col-md-8">
										<input type="text" name="region" class="form-control">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group text-right">
									 <div class="col-md-12">
									 	<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Add Appointment</button>
									 </div>

								</div>
							</div>
							
						{!! Form::close() !!}
							
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$("input:file").change(function (){
		   	$('.photo-overlay h3').text('Ready for upload...')
		});
	</script>
@stop
