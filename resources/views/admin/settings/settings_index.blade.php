@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Settings
					<div class="action-btns pull-right">
						<div class="btn-group">
							<a href="{{ url('/admin/settings/edit') }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
						<div class="col-md-6">
							<div class="body-item">
								@foreach ($settings as $setting)
									<div class="col-md-4">
										<p class="text-right">{{ $setting->name_setting }} :</p>
									</div>
									<div class="col-md-8">
										<p>{{ $setting->name_value }}</p>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
