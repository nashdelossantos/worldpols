@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Settings</div>

				<div class="panel-body">
					<div class="col-md-12">
						<div class="col-md-8">
							<div class="body-item">
								{!! Form::open(['url' => 'admin/settings/settings', 'method' => 'put']) !!}
									@foreach ($setting as $s)
										<div class="form-group">
											<label class="form-label col-md-3">{{ $s->name_setting }}</label>
											<div class="col-md-9">
												<input type="text" name="{{ $s->slug }}" value="{{ $s->name_value }}" class="form-control">
											</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
									<div class="form-group text-right">
										<div class="col-md-12">
											<button class="btn btn-primary"><i class="fa fa-cogs"></i> Update Settings</button>
										</div>
									</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
