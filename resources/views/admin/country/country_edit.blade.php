@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Countries
					<div class="action-btns pull-right">
						<div class="btn-group">
							<a href="{{ url('/admin/countries') }}" class="btn btn-sm btn-default"><i class="fa fa-mail-reply"></i> Cancel</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
					{!! Form::model($country, ['url' => 'admin/countries/' . $country->initial, 'files' => true, 'method' => 'PATCH']) !!}
						<div class="col-md-6">
							<div class="body-item">
								<h5>Editing a Country</h5>
								
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Initials :</label>
									<div class="col-md-9">
										{!! Form::text('initial', null, ['class' => 'form-control uppercase','id' => 'initials']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Name :</label>
									<div class="col-md-9">
										{!! Form::text('country_name', null, ['class' => 'form-control capitalize']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Capital :</label>
									<div class="col-md-9">
										{!! Form::text('capital', null, ['class' =>'form-control capitalize']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group hidden">
									<label class="control-label col-md-3 text-right">Capital ID :</label>
									<div class="col-md-9">
										{!! Form::text('capital_id', null, ['class' =>'form-control capitalize']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Time Zone :</label>
									<div class="col-md-9">
										{!! Form::selectRange('timezone', -12,+12, Input::old('timezone'), ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Active :</label>
									<div class="col-md-9">
										{!! Form::radio('active', 'on') !!}
									</div>
									<div class="clearfix"></div>
								</div>
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="col-md-12">
								<div class="country-img-holder">
									<div class="row">
										<div class="col-md-6"><img src="{{ asset('images/maps/') }}/{{ strtolower($country->initial) }}.jpg" alt="" id="map-img" class="img-responsive"></div>
										<div class="col-md-6"><img src="{{ asset('images/flags/') }}/{{ strtolower($country->initial) }}.png" alt="" id="flag-img" class="img-responsive"></div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 text-right">Language :</label>
								<div class="col-md-9">
									{!! Form::text('language', null, ['class' => 'form-control capitalize']) !!}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 text-right">Government :</label>
								<div class="col-md-9">
									{!! Form::text('government', null, ['class' => 'form-control capitalize']) !!}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 text-right">Population :</label>
								<div class="col-md-9">
									{!! Form::text('population', null, ['class' => 'form-control capitalize']) !!}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 text-right">Area :</label>
								<div class="col-md-9">
									{!! Form::text('area', null, ['class' => 'form-control capitalize']) !!}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group text-right">
								<div class="col-md-12">
									<button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Add Country</button>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script>
	$('#initials').focusout(function(){
		var initial = ($(this).val()).toLowerCase();

		$('#flag-img').attr('src', '/images/flags/'+initial+'.png');
		$('#map-img').attr('src', '/images/maps/'+initial+'.jpg');
	});
	</script>
@stop
