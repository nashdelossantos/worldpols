@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Countries
					<div class="action-btns pull-right">
						<div class="btn-group">
							<a href="{{ url('/admin/countries/create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> New</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
							<div class="body-item">
								<h5>List of Countries</h5>
									@foreach ($countries as $country)
										<div class="flag-holder">
											<a href="{{ url('/admin/countries/'.$country->initial.'/edit') }}"> 
												<img src="{{ asset('images/flags/') }}/{{ strtolower($country->initial) }}.png" alt="" class="img-responsive">
												 
												<p class="flag-name text-center">{{ $country->country_name }}</p>
											</a>
										</div>
									@endforeach
								</ul>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
