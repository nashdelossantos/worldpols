@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Countries
					<div class="action-btns pull-right">
						<div class="btn-group">
							<a href="{{ url('/admin/countries') }}" class="btn btn-sm btn-default"><i class="fa fa-mail-reply"></i> Cancel</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
					{!! Form::open(['url' => '/admin/countries', 'files' => true]) !!}
						<div class="col-md-6">
							<div class="body-item">
								<h5>Adding New Country</h5>
								
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Initials :</label>
									<div class="col-md-9">
										<input type="text" name="initial" class="form-control uppercase" id="initials">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Name :</label>
									<div class="col-md-9">
										<input type="text" name="country_name" class="form-control capitalize">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Capital :</label>
									<div class="col-md-9">
										<input type="text" name="capital" class="form-control capitalize">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Time Zone :</label>
									<div class="col-md-9">
										<select name="timezone" class="form-control">
											@for ($i = - 12; $i <= +12; $i++)
												<option value="@if ($i > 0)+@endif{{ $i }}">@if ($i > 0)+@endif{{ $i }}</option>
											@endfor
										</select>
											
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 text-right">Active :</label>
									<div class="col-md-9">
										<div class="checkbox">
											<label>
												<input type="checkbox" name="active"> <em>(if ticked, it will be available for search)</em>
											</label>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="col-md-12">
								<div class="country-img-holder">
									<div class="row">
										<div class="col-md-6"><img src="{{ asset('images/maps/no-map.jpg') }}" alt="" id="map-img" class="img-responsive"></div>
										<div class="col-md-6"><img src="{{ asset('images/flags/no-flag.jpg') }}" alt="" id="flag-img" class="img-responsive"></div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 text-right">Language :</label>
								<div class="col-md-9">
									<input type="text" name="language" class="form-control">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 text-right">Government :</label>
								<div class="col-md-9">
									<input type="text" name="government" class="form-control">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 text-right">Population :</label>
								<div class="col-md-9">
									<input type="text" name="population" class="form-control">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 text-right">Area :</label>
								<div class="col-md-9">
									<input type="text" name="area" class="form-control">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group text-right">
								<div class="col-md-12">
									<button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Add Country</button>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script>
	$('#initials').focusout(function(){
		var initial = ($(this).val()).toLowerCase();

		$('#flag-img').attr('src', '/images/flags/'+initial+'.png');
		$('#map-img').attr('src', '/images/maps/'+initial+'.jpg');
	});
	</script>
@stop
