@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">States
					<div class="action-btns pull-right">
						<div class="btn-group">
							<a href="{{ url('/admin/states/create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> New</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
						<div class="col-md-6">
							<div class="body-item">
								<ul>
									@foreach ($states as $state)
										<li><a href="{{ url('/admin/states/'.$state->slug.'/edit') }}"> {{ $state->state_name }}</a></li>
									@endforeach
								</ul>

							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
