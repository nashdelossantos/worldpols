@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Creating New State
					<div class="action-btns pull-right">
						<div class="btn-group">
							<a href="{{ url('/admin/states') }}" class="btn btn-sm btn-default"><i class="fa fa-arrow-circle-o-left"></i> Cancel</a>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
						
						{!! Form::model($states, ['url' => '/admin/states/'.$states->slug, 'method' =>'PATCH']) !!}
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4 text-right">Country : </label>
									<div class="col-md-8">
										{!! Form::select('country_id', $countries, null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 text-right">State Name : </label>
									<div class="col-md-8">
										{!! Form::text('state_name', null, ['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<div class="text-right col-md-12">
										{!! Form::submit('Add a State', ['class' => 'btn btn-primary']) !!}
									</div>
								</div>

							</div>
							
						{!! Form::close() !!}
							
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
