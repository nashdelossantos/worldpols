@extends('master.pages')

@section('subcontent')

	@section('page-title')Search Results @stop

	<section id="resultcontents" class="wow fadeInUp">
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-9 item-block">
					<h4>Viewing Person's Appointment Detail : </h4>

					<div class="col-md-12">
						<div class="item">
							<table class="table table-striped table-bordered">
								<tr>
									<td class="text-right">Name :</td>
									<td><b>{{ $person->full_name }}</b></td>
								</tr>
								<tr>
									<td class="text-right">Position :</td>
									<td><b>{{ $person->position_name }}</b></td>
								</tr>
								<tr>
									<td class="text-right">Country :</td>
									<td><b>{{ $person->country_name }}</b></td>
								</tr>
								<tr>
									<td class="text-right">State :</td>
									<td><b>{{ $person->state }}</b></td>
								</tr>
								<tr>
									<td class="text-right">Capital :</td>
									<td><b>{{ $person->capital }}</b></td>
								</tr>
								<tr>
									<td class="text-right">Region</td>
									<td> <b>{{ $person->region }}</b></td>
								</tr>
								<tr>
									<td class="text-right">Party</td>
									<td> <b>{{ $person->party }}</b></td>
								</tr>
								<tr>
									<td class="text-right">First Term</td>
									<td> <b>{{ $person->term_a }}</b></td>
								</tr>
								<tr>
									<td class="text-right">Second Term</td>
									<td> <b>{{ $person->term_b }}</b></td>
								</tr>
								<tr>
									<td class="text-right">Month of Inauguration</td>
									<td> <b>{{ date('F', mktime(0, 0, 0, $person->month, 10)) }}</b></td>
								</tr>
								<tr>
									<td class="text-right">Time of Office</td>
									<td> <b>{{ $person->office_time }} year/s</b></td>
								</tr>
								<tr>
									<td class="text-right">Term</td>
									<td> <b>{{ $person->term }} year/s</b></td>
								</tr>
								<tr>
									<td class="text-right">Elected</td>
									<td> <b>{{ $person->elect }}</b></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sidebar" id="person-details">
						<img src="{{ asset('images/people/') }}/{{ $person->avatar }}" alt="Nigeria" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>
@stop
