@extends('master.pages')

@section('subcontent')

	@section('page-title')Search Results @stop

	<section id="resultcontents" class="wow fadeInUp">
		<div class="container">
			<div class="col-md-12">
				@if (count($result) <= 0)
					<div class="alert alert-danger">Sorry, either we are currently updating or there are no existing appointments for the search criteria you provided.</div>
				@else
					<div class="col-md-9 item-block">
						<div class="col-md-12 left-item">
							<div class="criteria">
								<h4>Search Results </h4>
								<p>Year: {{ $year }}</p>
								<p>Country: {{ $country }}</p>
								<p>Position: {{ $position->position_name }}</p>
							</div>
						</div>

						<div class="col-md-12 left-item">
							<div class="item">
								<ul class="namelist">
									@foreach ($result as $r)
										<div class="media">
											<div class="media-left media-middle">
												<a href="search/{{ $r->slug }}"><img class="media-object" src="{{ asset('images/people/') }}/{{ $r->avatar }}" alt="position photo"></a>
											</div>
											<div class="media-body">
												<h4 class="media-heading"><a href="search/{{ $r->slug }}">{{ $r->full_name }}</a></h4>
												<p>Party: {{ $r->party }}</p>
												<p>Term: {{ $r->term_a }}</p>
												<p>Elected: {{ $r->elect }}</p>
												<p class="text-right">
													<a href="search/{{ $r->slug }}" class="btn btn-primary btn-xs">Read More</a>
												</p>
											</div>
										</div>
									@endforeach
								</ul>
							</div>
						</div>
					
					</div>
					<div class="col-md-3">
						<div class="sidebar" id="map-details">
							<img src="{{ asset('/images/flags') }}/{{ strtolower($countrydetails->flag) }}" alt="Nigeria" class="img-responsive">
							<div class="map-holder">
								<div class="map-overlay">
									<div>Temperature:<br/><span class="temp value"></span></div>
									<div>Pressure: <br/><span class="pressure value"></span></div>
								</div>
								<img src="{{ asset('/images/maps') }}/{{ strtolower($countrydetails->map) }}" alt="Nigeria" class="img-responsive">
							</div>
							<div>Name : <span><b>{{ $countrydetails->country_name }}</b></span></div>
							<div>Capital : <span><b>{{ $countrydetails->capital }}</b></span></div>
							<div>Language : <span><b>{{ $countrydetails->language }}</b></span></div>
							<div>Population : <span><b>{{ $countrydetails->population }}</b></span></div>
							<div>Area : <span><b>{{ $countrydetails->area }}</b></span></div>

							<input type="hidden" id="capitalid" value="{{ $countrydetails->capital }}">
						</div>
					</div>
				@endif
			</div>
		</div>
	</section>
@stop


@section('scripts')
	<script>
		$(function(){
			var capID = $('#capitalid').val();

			$.getJSON("http://api.openweathermap.org/data/2.5/weather?q="+capID, function(result){
				$('.temp').text(result.main.temp);
				$('.pressure').text(result.main.pressure);
		    });
		});
	</script>
@stop