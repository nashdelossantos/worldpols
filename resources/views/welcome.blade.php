@extends('master.base')

@section('styles')
	<link rel="stylesheet" href="{{ asset('css/jquery-jvectormap-2.0.2.css') }}">
@stop

@section('content')

	<section id="instruction">
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-4">
					<div class="thumbnail  wow fadeInLeft">
						<div class="big-icon"><i class="fa fa-search"></i></div>
						
						<div class="caption">
							<h3>Search</h3>
							<p class="texts">Choose an area from the map</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					 <div class="thumbnail wow fadeIn">
						<div class="big-icon"><i class="fa fa-list"></i></div>
						<div class="caption">
							<h3>Positions</h3>
							<p class="texts">Choose a position from the list</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="thumbnail wow fadeInRight">
						<div class="big-icon"><i class="fa fa-desktop"></i></div>
						<div class="caption">
							<h3>View</h3>
							<p class="texts">Get the results displayed</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div id="world-map" style="width: 100%; height: 400px" class="wow fadeIn">
		<div class="overlay"></div>
	</div>

	<h2 id="intro-text" class="text-center">Select a country to begin</h2>

	<div id="empty" class="col-md-12 wow fadeInUp">
		<div class="col-md-4 col-md-offset-4">
			<div class="alert alert-danger text-center"><i class="fa fa-info-circle"></i> Nothing to display for this country for now!</div>
		</div>
	</div>
	
	<section id="selects" class="wow fadeInUp">
		<div class="col-md-12">
			<div class="col-md-4 col-md-offset-4">
				<div class="text-center">
					<h2>Searching officials for <span class="country-name"></span></h2>
					
					{!! Form::open(array('url' => '/search')) !!}
						{!! Form::select('positions', $positions, null, ['class' => 'form-control']) !!}
						{!! Form::select('states', $states, null, ['class' => 'form-control hidden']) !!}
						{!! Form::selectRange('year', 1999,2015, null, ['class' => 'form-control']) !!}
						{!! Form::reset('Reset', ['class' => 'btn btn-danger']) !!}
						{!! Form::submit('Start Search', ['class' => 'btn btn-primary']) !!}
						<input name="withstate" type="hidden" value="">
						{!! Form::hidden('country') !!}
					{!! Form::close() !!}
				</div>
			</div>
			
		</div>
		<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
		      	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Missing Required Criteria!</h4>
		      	</div>
		      	<div class="modal-body">
					<p>Position and Year cannot be empty. Please select a position and year to search.</p>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Got It!</button>
		      	</div>
	    	</div>
	  	</div>
	</div>
		<div class="clearfix"></div>
	</section>
	
@stop

@section('scripts')
	<script type="text/javascript" src="{{ asset('js/jquery-jvectormap-2.0.2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery-jvectormap-world-mill-en.js') }}"></script>
	<script>
    	$(function(){
    		

    		var polData = <?php echo json_encode($countries); ?>;

			var countDef = <?php echo json_encode($countrynames); ?>;

		  	$('#world-map').vectorMap({
		    	map: 'world_mill_en',
		    	backgroundColor: '#5F8091',
		    	zoomButtons : false,
		    	zoomOnScroll : false,
		    	series: {
		      		regions: [{
		        		values: polData,
		        		scale: ['#C8EEFF', '#0071A4'],
		        		normalizeFunction: 'polynomial'
		      		}]
		    	},
		    	onRegionTipShow: function(e, el, code){
		    		var polPop = !polData[code] ? 'zone not set' : polData[code];
		      		el.html(el.html()+' ('+polPop+')');
		    	},
		    	onRegionClick: function(e, code) {
		    		$('#intro-text').hide();

		    		// add country name
		    		$('.country-name').text(countDef[code]);
					
					// if selected country has lists
		    		if (code in polData) { 
		    			$('#world-map').css('height', '100px');
		    			$('#world-map .overlay').show().css('height', '100px');
		    			$('#selects').show();
		    			$('#empty').hide();
		    			$('input[name=country]').val(code);
		    		} else {
		    			$('#selects').hide();
		    			$('#empty').show();
		    		}
		    		
		    	}
		  	});

			//show states option is other than president is selected
			$('select[name=positions]').on('change', function(){
				var posval = $(this).val();
				var presid = <?php echo json_encode($pres_id); ?>;

				if (presid == posval) {
					$('select[name=states]').addClass('hidden');
					$('input[name=withstate]').val(false);
				} else {
					$('select[name=states]').removeClass('hidden');
					$('input[name=withstate]').val(true);
				}
			});

			//before submit
			$('input[type=submit]').click(function(e){
				e.preventDefault();

				var position 	= $('select[name=positions]').val();
				var year 		= $('select[name=year]').val();

				if (position == 0 || year == 0) {
					$('#alertModal').modal('show');
				} else {
					$('form').submit();
				}

			});

		  	// button reset
		  	$('input[type=reset]').click(function(e) {
		  		$('#world-map').css('height', '400px');
		    	$('#world-map .overlay').hide().css('height', '100%');
		    	$('#selects').hide();
		    	$('#intro-text').show();
		  	});
		});
  	</script>
@stop