@extends('master.pages')

@section('content')
	<section id="subpage">
		<div class="container">
			<div class="col-md-12">
				<div class="title_page wow fadeInUp">
					<h3 class="text-right">{{ $content->page_title }}</h3>
				</div>
			</div>
		</div>
	</section>

	<section id="pagecontent">
		<div class="container">
			<div class="col-md-12">
				<div class="item">
					<h3>{{ $content->page_title }}</h3>
						{!! $content->page_content !!}
						
						@if ($pagename == 'contact-us')
							<div id="contact-us">
								<div class="col-md-6">
									{!! Form::open() !!}
										<div class="form-group">
											<label class="control-label col-md-3">First Name</label>
											<div class="col-md-9">
												{!! Form::text('firstname', null, ['class' => 'form-control']) !!}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Last Name</label>
											<div class="col-md-9">
												{!! Form::text('lastname', null, ['class' => 'form-control']) !!}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Phone</label>
											<div class="col-md-9">
												{!! Form::text('phone', null, ['class' => 'form-control']) !!}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Email Address</label>
											<div class="col-md-9">
												{!! Form::email('email', null, ['class' => 'form-control']) !!}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Enquiry</label>
											<div class="col-md-9">
												{!! Form::textarea('enquiry', null, ['class' => 'form-control']) !!}
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="form-group text-right">
											<div class="col-md-12">
												{!! Form::submit('Submit Enquiry', ['class' => 'btn btn-primary']) !!}
											</div>
											<div class="clearfix"></div>
										</div>
									{!! Form::close() !!}
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</div>
						@endif
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</section>
@stop