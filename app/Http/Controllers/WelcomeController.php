<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pages;
use App\Setting;
use App\Appointment;
use App\Position;
use App\States;
use App\Country;
use Mail;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| Public view, guest
	|
	*/

	protected $defaults;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');

		$defaults = Setting::get();

		$defarray = array();

		foreach ($defaults as $key => $value) {
			$defarray[$value['slug']] = $value['name_value'];
		}
		$this->defaults = $defarray;
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		date_default_timezone_set('Europe/London');
		$stdtime = date("H:i s");

		$countries =  Appointment::select('countries.initial', 'countries.country_name', 'timezone', DB::raw('count(appointments.id) as total'))
							->leftJoin('countries', 'appointments.country_id', '=', 'countries.id')
							->groupBy('appointments.country_id')
							->where('countries.active', '=', 'on')
							->get();

		$countrypol 	= array();
		$countrynames	= array();
		foreach ($countries as $key => $value) {
			//$countrypol[$value['initial']] = $value['total'];
			$countrypol[$value['initial']] = date("H:i:s",strtotime($value['timezone']." hours"));
			$countrynames[$value['initial']] = $value['country_name'];
		}

		$states 	= ['Choose a State'];
		$getstates 	= States::orderBy('state_name', 'asc')->get();

		foreach ($getstates as $key => $value) {
			$states[$value['id']] = $value['state_name'];
		}

		$positions = ['Choose a Position'];
		$presID = null;
		$getpositions = Position::orderBy('slug', 'ASC')->get();
		foreach ($getpositions as $key => $value) {
			if ($value['slug'] == 'president') $presID = $value['id'];

			$positions[$value['id']] = $value['position_name'];
		}

		//var_dump($countrypol);

		return view('welcome', 
			[
				'pagename' 		=> 'home', 
				'countries'		=> $countrypol,
				'countrynames'	=> $countrynames,
				'setting' 		=> $this->defaults,
				'positions'		=> $positions,
				'states' 		=> $states,
				'pres_id' 		=> $presID
			]);
	}

	public function show($slug)
	{
		$getpage = Pages::whereSlug($slug)->first();
		$pagename = $slug == 'contact-us' ? 'contact-us' : $slug;

		return view('page', 
			[
				'pagename' => $slug, 
				'content' => $getpage, 
				'pagename' => $pagename,
				'setting' 	=> $this->defaults
			]);
	}

	/**
	 * Show person's details
	 *
	 */
	public function showsearch($slug)
	{
		$getperson = DB::table('appointments')
						->join('countries', 'appointments.country_id', '=', 'countries.id')
						->join('positions', 'appointments.position_id', '=', 'positions.id')
						->select(
							"appointments.full_name", 
							"appointments.slug",
							"appointments.state",
							"appointments.capital",
							"appointments.region",
							"appointments.party",
							"appointments.term_a",
							"appointments.term_b",
							"appointments.month",
							"appointments.office_time",
							"appointments.term",
							"appointments.elect",
							"appointments.avatar",
							"countries.country_name",
							"positions.position_name"
						)
						->where('appointments.slug', '=', $slug)
						->first();
		$pagename = 'home';

		return view('person-view', 
			[
				'pagename' => $pagename, 
				'person' => $getperson,
				'setting'	=> $this->defaults
			]);
	}


	public function post(Request $request)
	{
		if ($request->page == 'contact-us') {
			/*
			Mail::send('emails.enquiry', ['key' => 'value'], function($message) {
				$message->to('nashsaint@gmail.com', 'Recipient')->subject('Website  Enquiry');
				$message->from('enquiry@worldpols.com', 'Worldpols');
			});
			*/
			$firstname 	= $request->get('firstname');
			$lastname 	= $request->get('lastname');
			$phone	 	= $request->get('phone');
			$email	 	= $request->get('email');
			$enquiry	= $request->get('enquiry');

			$message 	= "Firstname : ".$firstname.
							"\nLastname : ".$lastname.
							"\nPhone : ".$phone.
							"\nEmail : ".$email.
							"\nEnquiry :".$enquiry;
			$send = mail("nashsaint@gmail.com", "Website Enquiry", $message);

			if ($send) {
				return view('success', 
					[
						'pagename' 	=> 'contact-us', 
						'message' 	=> 'Email successfully sent, we will get back to you as soon as possible!',
						'setting'	=> $this->defaults
					]);
						
			} else {
				return  'failed';
			}
		} else {
			// get user search params

			$year 		= $request->get('year');
			$p 			= $request->get('positions');
			$position 	= Position::whereId($p)->first();

			$getcountry = Country::where('initial', '=', $request->get('country'))->first();

			$pagetitle  = 'Search Results';
			$pagename 	= 'home';

			//other than president
			if ($request->get('withstate') == 'true') {
				$getstate = $request->get('states');

				if ($getstate == 0) { // if no state was selected
					$result = DB::table('appointments')
								->join('countries', 'appointments.country_id', '=', 'countries.id')
								->join('positions', 'appointments.position_id', '=', 'positions.id')
								->join('states', 'appointments.state', '=', 'states.id')
								->select(
									"appointments.full_name", 
									"appointments.slug",
									"appointments.party",
									"appointments.term_a",
									"appointments.elect",
									"appointments.avatar",
									"states.state_name"
								)
								->where('appointments.position_id', '=', $p)
								->where('appointments.term_a', '=', $year)
								->get();
				} else { // include state in the search criteria
					$result = DB::table('appointments')
								->join('countries', 'appointments.country_id', '=', 'countries.id')
								->join('positions', 'appointments.position_id', '=', 'positions.id')
								->join('states', 'appointments.state', '=', 'states.id')
								->select(
									"appointments.full_name", 
									"appointments.slug",
									"appointments.party",
									"appointments.term_a",
									"appointments.elect",
									"appointments.avatar",
									"states.state_name"
								)
								->where('appointments.position_id', '=', $p)
								->where('appointments.term_a', '=', $year)
								->where('appointments.state', '=', $getstate)
								->get();
				}

					
			} else { // if only president
				dd('president');
				$result = DB::table('appointments')
							->join('countries', 'appointments.country_id', '=', 'countries.id')
							->join('positions', 'appointments.position_id', '=', 'positions.id')
							->select(
								"appointments.full_name", 
								"appointments.slug",
								"appointments.party",
								"appointments.term_a",
								"appointments.avatar",
								"appointments.elect"
							)
							->where('appointments.position_id', '=', $p)
							->where('appointments.term_a', '=', $year)
							->get();
			}

			return view('search-result', 
				[
					'result' 			=> $result,
					'year' 				=> $year,
					'position'			=> $position,
					'country'			=> $request->get('country'),
					'pagetitle'			=> $pagetitle,
					'pagename'			=> $pagename,
					'setting'			=> $this->defaults,
					'countrydetails' 	=> $getcountry
				]);
				
		}
	}

}
