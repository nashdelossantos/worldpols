<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\States;
use App\Country;

class StatesController extends Controller {

	protected $pagename;

	public function __construct()
	{
		$this->middleware('auth');
		$this->pagename = 'states';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$getstates = States::orderBy('slug')->get();
		return view('admin.states.states_index',
			[
				'states'	=> $getstates,
				'pagename' 	=> $this->pagename
			]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$countries = ["Choose a Country"];
		$getcountries = Country::orderBy('initial')->get();

		foreach ($getcountries as $key => $value) {
			$countries[$value['id']] = $value['country_name'];
		}

		return view('admin.states.states_create',
			[
				'pagename' 	=> $this->pagename,
				'countries'	=> $countries
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$country_id 		= $request->get('country_id');
		$site_name 			= $request->get('state_name');

		$state 				= new States;
		$state->slug 		= strtolower(str_replace(' ', '-', $site_name));
		$state->country_id	= $country_id;
		$state->state_name 	= $site_name;
		$state->save();

		$getstates = States::orderBy('state_name', 'asc')->get();

		return view('admin.states.states_index',
			[
				'pagename' 	=> $this->pagename,
				'states' 	=> $getstates
			]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function edit($slug)
	{
		$getStates = States::where('slug', '=', $slug)->first();

		$countries = ["Choose a Country"];
		$getcountries = Country::orderBy('initial')->get();

		foreach ($getcountries as $key => $value) {
			$countries[$value['id']] = $value['country_name'];
		}

		return view('admin.states.states_edit',
			[
				'pagename' 	=> $this->pagename,
				'states'	=> $getStates,
				'countries'	=> $countries
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $slug
	 * @return Response
	 */
	public function update(Request $request, $slug)
	{
		$country_id 		= $request->get('country_id');
		$site_name 			= $request->get('state_name');

		$state = States::where('slug', '=', $slug)->first();
		$state->slug 		= strtolower(str_replace(' ', '-', $site_name));
		$state->country_id	= $country_id;
		$state->state_name 	= $site_name;
		$state->save();

		$getstates = States::orderBy('state_name', 'asc')->get();

		return view('admin.states.states_index',
			[
				'pagename' 	=> $this->pagename,
				'states' 	=> $getstates
			]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
