<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Illuminate\Http\Request;
use App\Setting;
use Redirect;

class SettingController extends Controller {

	protected $pagename;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->pagename = 'settings';
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$settings = Setting::all();
		return view('admin.settings.settings_index', 
			[
				'pagename' => $this->pagename,
				'settings' => $settings
			]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$settings = Setting::all();
		return view('admin.settings.settings_edit', 
			[
				'pagename' => $this->pagename,
				'setting' => $settings
			]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $slue
	 * @return Response
	 */
	public function edit($slug)
	{
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function update(Request $request, $slug)
	{
		$input = $request->all();

		foreach ($input as $key => $value) {
			if (!preg_match('/_/', $key)) {

				$setting = new Setting;
				$setting = Setting::where('slug', '=', $key)->first();

				$setting->name_value 	= $value;
				$setting->save();
			}
		}
		return Redirect::to('/admin/settings');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
