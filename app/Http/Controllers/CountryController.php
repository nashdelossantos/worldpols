<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Country;
use Redirect;

class CountryController extends Controller {

	protected $pagename;

	public function __construct()
	{
		$this->middleware('auth');
		$this->pagename = 'countries';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Country $country)
	{
		$countries = Country::orderBy('country_name', 'asc')->get();

		return view('admin.country.country_index',
			[
				'pagename' 	=> $this->pagename,
				'countries'	=> $countries
			]
		);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.country.country_create',
			[
				'pagename' 	=> $this->pagename
			]
		);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$initial 		= $request->get('initial');
		
		$country_name 	= $request->get('country_name');
		$capital 		= $request->get('capital');
		$capital_id 	= $request->get('capital_id');
		$language 		= $request->get('language');
		$government 	= $request->get('government');
		$population 	= $request->get('population');
		$area 			= $request->get('area');
		$active 		= $request->get('active');
		$timezone		= $request->get('timezone');
		$now 			= date('Y-m-d H:i:s');

		$country 				= new Country;
		$country->initial 		= strtoupper($initial);
		$country->timezone 		= $timezone;
		$country->country_name 	= ucwords($country_name);
		$country->capital 		= $capital;
		$country->capital_id 	= $capital_id;
		$country->language 		= $language;
		$country->government 	= $government;
		$country->population 	= $population;
		$country->area 			= $area;
		$country->active 		= $active;
		$country->created_at 	= $now;
		$country->updated_at 	= $now;
		$country->save();

		return Redirect::to('/admin/countries');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function edit($slug)
	{
		$getCountry = Country::where('initial', '=', $slug)->first();

		return view('admin.country.country_edit',
			[
				'pagename' 	=> $this->pagename,
				'country'	=> $getCountry
			]
			
		);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update(Request $request, $slug)
	{
		$initial 		= $request->get('initial');
		
		$country_name 	= $request->get('country_name');
		$capital 		= $request->get('capital');
		$capital_id 	= $request->get('capital_id');
		$language 		= $request->get('language');
		$government 	= $request->get('government');
		$population 	= $request->get('population');
		$area 			= $request->get('area');
		$active 		= $request->get('active');
		$timezone		= $request->get('timezone');
		$now 			= date('Y-m-d H:i:s');

		$country = Country::where('initial', '=', $slug)->first();

		//process files
		if ($request->hasFile('map')) {
			$extension = $request->file('map')->getClientOriginalExtension();
			$mapimg = strtolower($initial).'.'.$extension;
			$destinationPath = 'images/maps';
		    $request->file('map')->move($destinationPath, $mapimg);
		    $country->map = $mapimg;
		} 
		if ($request->hasFile('flag')) {
			$extension = $request->file('flag')->getClientOriginalExtension();
			$flagimg = strtolower($initial).'.'.$extension;
			$destinationPath = 'images/flags';
		    $request->file('flag')->move($destinationPath, $flagimg);
		    $country->flag = $flagimg;
		}

		$country->initial 		= strtoupper($initial);
		$country->timezone 		= $timezone;
		$country->country_name 	= ucwords($country_name);
		$country->capital 		= $capital;
		$country->capital_id 	= $capital_id;
		$country->language 		= $language;
		$country->government 	= $government;
		$country->population 	= $population;
		$country->area 			= $area;
		$country->active 		= $active;
		$country->created_at 	= $now;
		$country->updated_at 	= $now;
		$country->save();

		return Redirect::to('/admin/countries');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function rules()
	{
	    return [
	      'name'        => 'required',
	      'sku'         => 'required|unique:products,sku,' . $this->get('id'),
	      'image'       => 'required|mimes:png,jpg,jpeg'
	    ];
	}

}
