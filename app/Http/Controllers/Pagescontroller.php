<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\Pages;

class Pagescontroller extends Controller {

	protected $pagename;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->pagename = 'pages';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pagename = 'pages';
		$pages = Pages::take(10)->orderBy('updated_at', 'desc')->get();

		return view('admin.pages.pages_index', ['pages' => $pages, 'pagename' => $this->pagename]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string $slug
	 * @return Response
	 */
	public function edit($slug)
	{
		$pages = Pages::whereSlug($slug)->first();
		return view('admin.pages.pages_edit', ['pages' => $pages, 'pagename' => $this->pagename]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function update($slug, Request $request)
	{
		$pages = Pages::whereSlug($slug)->first();
		
		$pages->page_title		= $request->get('page_title');
		$pages->page_content 	= $request->get('page_content');

		$pages->save();

		return Redirect::to('/admin/pages');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
