<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Input;
use Redirect;
use App\Appointment;
use App\Country;
use App\States;
use App\Position;
use Session;
use Auth;

class AppointmentController extends Controller {

	protected $pagename;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->pagename = 'appointments';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Appointment $appointment)
	{
		$appointments = Appointment::where('slug', 'like', 'a%')->orderBy('full_name', 'asc')->get();
		$columns = (round(((int)(sizeof($appointments)))/3));

		return view('admin.appointment.appointment_index', 
			[
				'appointments' => $appointments, 
				'pagename' => $this->pagename,
				'collimit' 	=> $columns,
				'currpage' 	=> 'a'
			]);

		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// get countries and format array for select
		$getcountries = Country::where('active', '=', 'on')->orderBy('country_name', 'asc')->get();
		$country = ["Select Position"];

		foreach ($getcountries as $key => $value) {
			$country[$value['id']] = $value['country_name'];
		}

		//get states and format for select
		$getstates = States::orderBy('slug', 'asc')->get();
		$states = ["Select a State"];
		foreach ($getstates as $key => $value) {
			$states[$value['id']] = $value['state_name'];
		}
		
		$getpositions = Position::orderBy('slug', 'asc')->get();
		$positionarr = ["Choose a Position"];
		foreach ($getpositions as $key => $value) {
			$positionarr[$value['id']] = $value['position_name'];
		}

		return view('admin.appointment.appointment_create', 
			[
				'country' 		=> $country, 
				'states' 		=> $states,
				'positionarr' 	=> $positionarr, 
				'pagename' 		=> $this->pagename
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// validate
        $rules = array(
            'full_name'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('/admin/appointment/create')
                ->withErrors($validator)
                ->withInput();
        } else {

        	$appoint = new Appointment;
        	$slug = strtolower(str_replace(' ', '-', Input::get('full_name')));

        	//check if avatar is present
        	if (Input::hasFile('avatar') && Input::file('avatar')->isValid())
			{
				$extension 			= Input::file('avatar')->getClientOriginalExtension();
				$avatarfilename		= $slug.'.'.$extension;
				$destinationPath 	= 'images/people';
				Input::file('avatar')->move($destinationPath, $avatarfilename);

				$appoint->avatar     	= $avatarfilename;
			}

            $appoint->full_name     = Input::get('full_name');
            $appoint->position_id   = Input::get('position');
            $appoint->slug       	= $slug;
            $appoint->party      	= Input::get('party');
            $appoint->term_a      	= Input::get('term_a');
            $appoint->term_b      	= Input::get('term_b');
            $appoint->month      	= Input::get('month');
            $appoint->office_time   = Input::get('office_time');
            $appoint->term   		= Input::get('term');
            $appoint->elect   		= Input::get('elect');
            $appoint->state   		= Input::get('state');
            $appoint->country_id   	= Input::get('country');
            $appoint->capital   	= Input::get('capital');
            $appoint->region 	  	= Input::get('region');
            $appoint->save();

            // redirect
            Session::flash('message', 'Successfully created nerd!');
            return Redirect::to('/admin/appointment');
        }

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $appointment
	 * @return Response
	 */
	public function show(Appointment $appoint)
	{
		return view('admin.appointment.appointment_show');
	}

	/**
	 *
	 * @param  string $slug
	 * @return Response
	 */
	public function paginator($slug)
	{

		$appointments = Appointment::where('slug', 'like', $slug.'%')->orderBy('full_name', 'asc')->get();
		$columns = (round(((int)(sizeof($appointments)))/3));

		return view('admin.appointment.appointment_index', 
			[
				'appointments' => $appointments, 
				'pagename' => $this->pagename,
				'collimit' 	=> $columns,
				'currpage' 	=> $slug
			]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function edit($slug)
	{
		$appointment = Appointment::whereSlug($slug)->first();

		// get countries and format array for select
		$getcountries = Country::where('active', '=', 'on')->orderBy('country_name', 'asc')->get();
		$country = ["Select Position"];

		foreach ($getcountries as $key => $value) {
			$country[$value['id']] = $value['country_name'];
		}

		//get states and format for select
		$getstates = States::orderBy('slug', 'asc')->get();
		$states = ["Select a State"];
		foreach ($getstates as $key => $value) {
			$states[$value['id']] = $value['state_name'];
		}

		$getpositions = Position::orderBy('slug', 'asc')->get();
		$positionarr = ["Choose a Position"];
		foreach ($getpositions as $key => $value) {
			$positionarr[$value['id']] = $value['position_name'];
		}

		return view('admin.appointment.appointment_edit', 
			[
				'appointment' 	=> $appointment,
				'country'		=> $country,
				'states'		=> $states,
				'positionarr' 	=> $positionarr,
				'pagename' 		=> $this->pagename
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function update($slug, Request $request)
	{

		$appointment 	= Appointment::whereSlug($slug)->first();
		$avatar			= strtolower(str_replace(' ', '-', $request->get('full_name')));

		if (Input::hasFile('avatar') && Input::file('avatar')->isValid())
		{
			$extension 			= Input::file('avatar')->getClientOriginalExtension();
			$avatarfilename		= $slug.'.'.$extension;
			$destinationPath 	= 'images/people';
			Input::file('avatar')->move($destinationPath, $avatarfilename);

			$appointment->avatar     	= $avatarfilename;
		}
		
		$appointment->full_name 	= $request->get('full_name');
		$appointment->position_id 	= $request->get('position_id');
		$appointment->country_id 	= $request->get('country_id');
		$appointment->party 		= $request->get('party');
		$appointment->term_a 		= $request->get('term_a');
		$appointment->term_b 		= $request->get('term_b');
		$appointment->month 		= $request->get('month');
		$appointment->office_time 	= $request->get('office_time');
		$appointment->term 			= $request->get('term');
		$appointment->state 		= $request->get('state');
		$appointment->capital 		= $request->get('capital');
		$appointment->region 		= $request->get('region');

		$appointment->save();

		return Redirect::to('/admin/appointment');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
