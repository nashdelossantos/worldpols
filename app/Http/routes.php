<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('/home', 'HomeController@index');

Route::get('/{page}', 'WelcomeController@show');
Route::post('/{page}', 'WelcomeController@post');

Route::get('/search/{slug}', 'WelcomeController@showsearch');
Route::post('/search', 'WelcomeController@post');

Route::get('/admin', 'AdminController@index');

Route::resource('/admin/appointment', 'AppointmentController');
Route::get('/admin/appointment/page/{slug}', 'AppointmentController@paginator');

Route::resource('/admin/countries', 'CountryController');

Route::resource('/admin/pages', 'PagesController');

Route::resource('/admin/settings', 'SettingController');

Route::resource('/admin/states', 'StatesController');

//Route::get('/admin/appointment/new', 'AppointmentController@create');




Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
