<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;

    use App\Position;

    class PositionTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $country = array(
                ['position_name'      => 'Governor'],
                ['position_name'      => 'House of Representative'],
                ['position_name'      => 'Senator'],
                ['position_name'      => 'President'],
            );
            Position::insert($country);
        }
    }
?>