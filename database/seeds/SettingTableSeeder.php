<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\Setting;

    class SettingTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $setting = array(
                [
                    'slug'          => 'site-name',
                    'name_setting'  => 'Site Name',
                    'name_value'    => 'WorldPols',
                    'created_at'    => $now,
                    'updated_at'    => $now
                ],
                [
                    'slug'          => 'facebook',
                    'name_setting'  => 'Facebook',
                    'name_value'    => 'http://www.facebook.com',
                    'created_at'    => $now,
                    'updated_at'    => $now
                ],
                [
                    'slug'          => 'twitter',
                    'name_setting'  => 'Twitter',
                    'name_value'    => 'http://www.twitter.com',
                    'created_at'    => $now,
                    'updated_at'    => $now
                ]
            );
            Setting::insert($setting);
        }
    }
?>