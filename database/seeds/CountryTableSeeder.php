<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;

    use App\Country;

    class CountryTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $country = array(
                [
                    'initial'      => 'NG',
                    'country_name'          => 'Nigeria'
                    
                ]
            );
            Country::insert($country);
        }
    }
?>