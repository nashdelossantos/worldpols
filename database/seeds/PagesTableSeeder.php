<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;

    use App\Pages;

    class PagesTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $pages = array(
                [
                    'slug'          => 'about-us',
                    'page_title'    => 'About Us',
                    'page_content'  => 'Lorem Ipsum'
                ],
                [
                    'slug'          => 'contribute',
                    'page_title'    => 'Contribute',
                    'page_content'  => 'Contribute Page'
                ],
                [
                    'slug'          => 'contact-us',
                    'page_title'    => 'Contact Us',
                    'page_content'  => 'Lorem Ipsum'
                ],
            );
            Pages::insert($pages);
        }
    }
?>