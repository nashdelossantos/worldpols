<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\User;

    class UserTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $users = array(
                [
                    'username'      => 'codegap',
                    'firstname'     => 'Admin',
                    'lastname'      => 'Admin',
                    'avatar'        => 'blank',
                    'email'         => 'info@codegap.co.uk',
                    'password'      => Hash::make('pa55w0rd'),
                    'created_at'    => $now,
                    'updated_at'    => $now,
                    
                ]
            );
            User::insert($users);
        }
    }
?>