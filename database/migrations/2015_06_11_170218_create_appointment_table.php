<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('appointments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slug')->nullable();
			$table->string('position_id')->nullable();
			$table->string('country_id')->nullable();
			$table->string('state')->nullable();
			$table->string('capital')->nullable();
			$table->string('region')->nullable();
			$table->string('full_name')->nullable();
			$table->string('party')->nullable();
			$table->integer('term_a')->unsigned();
			$table->integer('term_b')->unsigned();
			$table->integer('month')->unsigned();
			$table->string('office_time')->nullable();
			$table->string('term')->nullable();
			$table->string('elect')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('appointments');
	}

}
