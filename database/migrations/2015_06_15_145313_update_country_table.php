<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries', function(Blueprint $table)
		{
			$table->string('flag')->after('country_name')->nullable();
			$table->string('map')->after('flag')->nullable();
			$table->string('capital')->after('map')->nullable();
			$table->string('capital_id')->after('capital')->nullable();
			$table->string('language')->after('capital_id')->nullable();
			$table->string('government')->after('language')->nullable();
			$table->string('population')->after('government')->nullable();
			$table->string('area')->after('population')->nullable();
			$table->string('temperature')->after('area')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries', function(Blueprint $table)
		{
			$table->dropColumn('flag');
			$table->dropColumn('map');
			$table->dropColumn('capital');
			$table->dropColumn('capital_id');
			$table->dropColumn('language');
			$table->dropColumn('government');
			$table->dropColumn('population');
			$table->dropColumn('area');
			$table->dropColumn('temperature');
		});
	}

}
